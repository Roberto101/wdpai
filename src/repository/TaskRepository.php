<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Task.php';

class TaskRepository extends Repository
{
    public function getTasks(string $date): array
    {
        session_start();
        $result = [];
        $name_user=$_SESSION['login_user'];
        /*$stmt = $this->database->connect()->prepare("
            SELECT t.id, t.id_user, t.title, t.description, t.add_date, t.date, t.time
            FROM tasks t
            INNER JOIN users u on  u.id=t.id_user
            WHERE u.email=?
        ");*/
        $stmt='';
        if($date){
            $stmt = $this->database->connect()->prepare("
            SELECT t.id, t.id_user, t.title, t.description, t.add_date, t.date, t.time
            FROM tasks t
            INNER JOIN users u on  u.id=t.id_user
            WHERE u.email=? AND t.date = ?
            ORDER BY t.time
            ");
            $stmt->execute([$name_user,$date]);
        }
        else{
            $stmt = $this->database->connect()->prepare("
            SELECT t.id, t.id_user, t.title, t.description, t.add_date, t.date, t.time
            FROM tasks t
            INNER JOIN users u on  u.id=t.id_user
            WHERE u.email=?
            ORDER BY t.time
        ");
            $stmt->execute([$name_user]);
        }

        $tasks = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($tasks as $task){
            $result[]=new Task(
                $task['id'],
                $task['id_user'],
                $task['title'],
                $task['description'],
                $task['add_date'],
                $task['date'],
                $task['time']
            );

        }

        return $result;

    }
    public function addTask(Task $task):void {
        session_start();
        date_default_timezone_set('Europe/Warsaw');
        $date = new DateTime();
        $stmt =$this->database->connect()->prepare('
            INSERT INTO tasks (id_user,title,description,add_date,date,time)
            VALUES (?,?,?,?,?,?)
        ');
        /*$iduser=1;*/
        $stmt->execute([
            $task->getIdUser(),
            $task->getTitle(),
            $task->getDescription(),
            $date->format('Y-m-d'),
            $task->getDate(),
            $task->getTime()
        ]);
    }

}