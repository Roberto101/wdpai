<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

class ToDosRepository extends Repository
{
    public function getToDos(string $date): array
    {
        session_start();
        $result = [];
        $name_user=$_SESSION['login_user'];
        $stmt='';
        if($date){
            $stmt = $this->database->connect()->prepare("
            SELECT t.id, t.id_user, t.title, t.description, t.add_date, t.completed, t.due_date
            FROM todos t
            INNER JOIN users u on  u.id=t.id_user
            WHERE u.email=? AND t.due_date = ?
            ORDER BY t.completed
            ");
            $stmt->execute([$name_user,$date]);
        }
        else{
            $stmt = $this->database->connect()->prepare("
            SELECT t.id, t.id_user, t.title, t.description, t.add_date, t.completed, t.due_date
            FROM todos t
            INNER JOIN users u on  u.id=t.id_user
            WHERE u.email=?
            ORDER BY t.completed
        ");
            $stmt->execute([$name_user]);
        }

        $todos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($todos as $todo){
            $result[]=new ToDo(
                $todo['id'],
                $todo['id_user'],
                $todo['title'],
                $todo['description'],
                $todo['add_date'],
                $todo['completed'],
                $todo['due_date']
            );

        }

        return $result;

    }
    public function addToDo(ToDo $todo):void {
        session_start();
        date_default_timezone_set('Europe/Warsaw');
        $date = new DateTime();
        $stmt =$this->database->connect()->prepare('
            INSERT INTO todos (id_user,title,description,add_date,due_date)
            VALUES (?,?,?,?,?)
        ');
        /*$iduser=1;*/
        $stmt->execute([
            $todo->getIdUser(),
            $todo->getTitle(),
            $todo->getDescription(),
            $date->format('Y-m-d'),
            $todo->getDueDate()
        ]);
    }

    public function completeToDO(int $id)
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE todos SET completed = true WHERE id= :id
        ');
        $stmt->bindParam(':id',$id,PDO::PARAM_INT);
        $stmt->execute();
    }

    public function undoToDO(int $id)
    {
        $stmt = $this->database->connect()->prepare('
            UPDATE todos SET completed = false WHERE id= :id
        ');
        $stmt->bindParam(':id',$id,PDO::PARAM_INT);
        $stmt->execute();
    }
}