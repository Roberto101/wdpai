<?php


class ToDo
{
    private $id;
    private $id_user;
    private $title;
    private $description;
    private $add_date;
    private $completed;
    private $due_date;


    public function __construct($id,$id_user,$title, $description,$add_date,$completed, $due_date)
    {
        $this->id = $id;
        $this->id_user = $id_user;
        $this->title = $title;
        $this->description = $description;
        $this->add_date = $add_date;
        $this->completed = $completed;
        $this->due_date = $due_date;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIdUser()
    {
        return $this->id_user;
    }


    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /*public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): void
    {
        $this->date = $date;
    }*/
    public function getAddDate()
    {
        return $this->add_date;
    }

    public function setAddDate($add_date): void
    {
        $this->add_date = $add_date;
    }

    public function getCompleted()
    {
        return $this->completed;
    }

    public function setCompleted($completed): void
    {
        $this->completed = $completed;
    }

    public function getDueDate()
    {
        return $this->due_date;
    }

    public function setDueDate($due_date): void
    {
        $this->due_date = $due_date;
    }

}