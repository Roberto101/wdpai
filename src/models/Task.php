<?php


class Task
{
    private $id;
    private $id_user;
    private $title;
    private $description;
    private $add_date;
    private $date;
    private $time;

    public function __construct($id,$id_user,$title, $description,$add_date, $date, $time)
    {
        $this->id = $id;
        $this->id_user = $id_user;
        $this->title = $title;
        $this->description = $description;
        $this->add_date = $add_date;
        $this->date = $date;
        $this->time = $time;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIdUser()
    {
        return $this->id_user;
    }


    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getAddDate()
    {
        return $this->add_date;
    }

    public function setAddDate($add_date): void
    {
        $this->add_date = $add_date;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date): void
    {
        $this->date = $date;
    }
    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time): void
    {
        $this->time = $time;
    }


}