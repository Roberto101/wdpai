<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Task.php';
require_once __DIR__.'/../repository/TaskRepository.php';

class TaskController extends AppController
{

    private $messages = [];
    private $TaskRepository;

    public function __construct()
    {
        parent::__construct();
        $this->TaskRepository = new TaskRepository();
    }
//!!!!!!generate what?
    public function tasks() {
        //TODO Display todos.php
        $tasks=$this->TaskRepository->getTasks('');
        #echo($_SESSION['login_user']);
        #echo($_SESSION['login_id']);

        $this->render('day',['tasks'=>$tasks]);
    }

    public function addTask(){

        if($this->isPost()){
            //todo
            session_start();

            $task = new Task(null,$_SESSION['login_id'],$_POST['title'],$_POST['description'],null,$_POST['date'],$_POST['time']);
            $this->TaskRepository->addTask($task);
            $tasks=$this->TaskRepository->getTasks();
            #return $this->render('day');
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/day");
        }
        $this->render('add_task',['messages'=>$this->messages]);
    }

}