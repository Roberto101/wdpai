<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController
{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login(){
        //$user = new User('test1@mail.pl', 'admin','Jan','Dzban');

        session_start();
        if(isset($_SESSION['login_user'])){
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/todos");
            #echo("there is a user");
        }

        $userRepository = new UserRepository();

        if(!$this->isPost()){
            return $this->render('login');
        }

        $email = $_POST["email"];
        $password = $_POST["password"];

        $user = $userRepository->getUser($email);
        if(!$user){
            return $this->render('login', ['messages'=> ['User does not exist']]);
        }

        if($user->getEmail() !== $email){
            return $this->render('login', ['messages'=> ['User does not exist']]);
        }

        if(!password_verify($password, $user ->getPassword())){
            return $this->render('login', ['messages'=> ['Password does not match']]);
        }

        $_SESSION['login_user']=$email;
        $_SESSION['login_id']=$user->getId();


        //return $this->render('week');
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/todos");

    }
    public function register(){
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $salt = "Salt";

        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }

        //TODO try to use better hash function
        //$user = new User($email, md5($password), $name, $surname, $salt);
        $user = new User($email, password_hash($password, PASSWORD_BCRYPT), $name, $surname, $salt);

        $this->userRepository->addUser($user);

        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }
    public function logout(){
        session_start();
        if(session_destroy()) // Destroying All Sessions
        {
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/login");
        }

    }
}