<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/ToDo.php';
require_once __DIR__.'/../repository/ToDosRepository.php';

class ToDosController extends AppController
{

    private $messages = [];
    private $ToDosRepository;

    public function __construct()
    {
        parent::__construct();
        $this->ToDosRepository = new ToDosRepository();
    }

    public function todos() {
        //TODO Display todos.php
        $todos=$this->ToDosRepository->getToDos('');
        #echo($_SESSION['login_user']);
        #echo($_SESSION['login_id']);

        $this->render('todos',['todos'=>$todos]);
    }

    public function addToDo(){

        if($this->isPost()){
            //todo
            session_start();

            $todo = new ToDo(null,$_SESSION['login_id'],$_POST['title'],$_POST['description'],null,null,$_POST['due_date']);
            $this->ToDosRepository->addToDo($todo);
            $todos=$this->ToDosRepository->getToDos('');
            return $this->render('todos',['todos'=>$todos]);;
        }
        $this->render('add_todo',['messages'=>$this->messages]);
    }

    public function completeToDo(int $id){
        $this->ToDosRepository->completeToDO($id);
        http_response_code(200);
    }
    public function undoToDo(int $id){
        $this->ToDosRepository->undoToDO($id);
        http_response_code(200);
    }
}