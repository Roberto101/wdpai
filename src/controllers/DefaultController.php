<?php

require_once 'AppController.php';

class DefaultController extends AppController {

   public function index() {
       //TODO Display login.php
       $this->render('login');
   }
   public function week() {
    //TODO Display week.php
    $this->render('week');
   }
    public function year() {
        //TODO Display year.php
        $this->render('year');
    }
    public function month() {
        //TODO Display year.php
        $this->render('month');
    }
    public function day() {
        //TODO Display day.php
        $this->render('day');
    }
    /*public function todos() {
        //TODO Display todos.php
        $this->render('todos');
    }*/
}