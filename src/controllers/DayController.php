<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/ToDo.php';
require_once __DIR__.'/../repository/ToDosRepository.php';
require_once __DIR__.'/../models/Task.php';
require_once __DIR__.'/../repository/TaskRepository.php';

class DayController extends AppController
{

    private $messages = [];
    private $ToDosRepository;
    private $TaskRepository;

    public function __construct()
    {
        parent::__construct();
        $this->ToDosRepository = new ToDosRepository();
        $this->TaskRepository = new TaskRepository();
    }

    public function day() {
        //TODO Display todos.php
        date_default_timezone_set('Europe/Warsaw');
        $date = new DateTime();
        #$date->format('Y-m-d');
        $todos=$this->ToDosRepository->getToDos($date->format('Y-m-d'));
        $tasks=$this->TaskRepository->getTasks($date->format('Y-m-d'));

        $this->render('day',['todos'=>$todos,'tasks'=>$tasks]);
    }
}