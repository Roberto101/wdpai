const checkInputs = document.querySelectorAll(".todo-checkbox");



function completeToDo(){
    const check = this;
    const fid=check.getAttribute("id");
    const id=fid.slice(4,fid.length)

    if(check.checked){

        fetch(`/completetodo/${id}`)
            .then(function (){
                //console.log("i am sending check",`/completetodo/${id}`)
                console.log(check)
                check.parentElement.classList.add('completed');

            //element.classList.remove('no-valid');
            })
    }
    else{
        fetch(`/undotodo/${id}`)
            .then(function (){
                //console.log("I am sending uncheck")
                check.parentElement.classList.remove('completed');
            })

    }
}
function isChecked(item){
    if(item.checked){
        item.parentElement.classList.add('completed');
    }
}

checkInputs.forEach(check=>check.addEventListener("change", completeToDo));
checkInputs.forEach(isChecked);