const nextMonth = document.querySelectorAll("#next")[0];
const previousMonth = document.querySelectorAll("#previous")[0];

var $calendar = document.getElementById("calendar");
var currentYear = new Date().getFullYear();
var currentMonth = new Date().getMonth();
var calendarize = new Calendarize();

calendarize.buildMonthCalendar($calendar, currentYear,currentMonth);


nextMonth.addEventListener("click", next_Month)
previousMonth.addEventListener("click", previous_Month)

function next_Month(){
    console.log('next')
    currentMonth+=1;
    if(currentMonth>11){
        currentMonth=0;
        currentYear+=1;
    }
    console.log(currentMonth,currentYear)
    $calendar.innerHTML="";
    calendarize.buildMonthCalendar($calendar, currentYear,currentMonth);
}
function previous_Month(){
    console.log('next')
    currentMonth-=1;
    if(currentMonth<0){
        currentMonth=11;
        currentYear-=1;
    }
    $calendar.innerHTML="";
    calendarize.buildMonthCalendar($calendar, currentYear,currentMonth);
}