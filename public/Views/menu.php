<?php
echo '
<img src="public/img/Logo.svg">
            <ul>
                <li>
                    <i class="fas fa-calendar-day"></i>
                    <a href="day" class="button">Today</a>
                </li>
                <li>
                    <i class="fas fa-calendar-week"></i>
                    <a href="week" class="button"> Week</a>
                </li>
                <li>
                    <i class="fas fa-calendar-alt"></i>
                    <a href="month" class="button"> Month</a>
                </li>
                <li>
                    <i class="fas fa-calendar"></i>
                    <a href="year" class="button"> Year</a>
                </li>

                <li>

                </li>
                <li>
                    <i class="fas fa-calendar-check"></i>
                    <a href="todos" class="button"> ToDo</a>
                </li>
                <li>
                    <i class="fas fa-clipboard-list"></i>
                    <a href="#" class="button"> Habits</a>
                </li>
                <li>
                    <i class="fas fa-sign-out-alt fa-flip-horizontal"></i>
                    <a href="logout" class="button"> Logout</a>
                </li>
            </ul>
'
?>