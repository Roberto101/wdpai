<?php include('./session.php');?>
<!DOCTYPE html>
<head>
    <title>Plandy</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/todos.css">
    <link href="https://pl.allfont.net/allfont.css?fonts=bookman-old-style" rel="stylesheet" type="text/css" />
    <script src="https://kit.fontawesome.com/6ff9b2a121.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/todos.js" defer></script>
    <script type="text/javascript" src="/public/js/menu.js" defer></script>

</head>
<body>
    <div class="base-container">
        <nav>
        <?php include('menu.php');?>
        </nav>
       <main>
           <header>
               <!--<div class="arrow"><i class="fas fa-caret-left"></i></div>-->
               <div class="menuButton" ><i class="fas fa-bars"></i></div>
               <div class="headerText">ToDo's</div>
               <div class="addButton">
                   <a href="addToDo">
                       <i class="fas fa-plus-square"></i>
                   </a>
               </div>

           </header>
           <section class="todos">
               <?php foreach ($todos as $todo):?>

                <div class="todo">
                    <input class = "todo-checkbox" <?= ($todo->getCompleted())?'checked ':""?> id="todo<?= $todo->getID() ?>" type="checkbox">
                    <label for="todo<?= $todo->getID() ?>"><?= $todo->getTitle() ?></label>
                    <span class="description"><?= $todo->getDescription() ?></span>
                    <span class="description"><?= strval($todo->getDueDate()) ?></span>
                </div>
               <? endforeach;?>
           </section>
       </main>
    </div>
</body>