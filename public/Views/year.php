<?php include('./session.php');?>
<!DOCTYPE html>
<head>
    <title>Plandy</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/calendar.css">
    <link rel="stylesheet" type="text/css" href="public/css/year.css">
    <link href="https://pl.allfont.net/allfont.css?fonts=bookman-old-style" rel="stylesheet" type="text/css" />
    <script src="https://kit.fontawesome.com/6ff9b2a121.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/public/js/calendarize.js" defer></script>
    <script type="text/javascript" src="/public/js/year.js" defer></script>
    <script type="text/javascript" src="/public/js/menu.js" defer></script>
</head>
<body>
<div class="base-container">
    <nav>
        <?php include('menu.php');?>
    </nav>
    <main>
        <header>
            <div class="menuButton" ><i class="fas fa-bars"></i></div>
            <div class="arrow" id="previous"><i class="fas fa-caret-left"></i></div>
            <div class="headerText">Year</div>
            <div class="arrow" id="next"><i class="fas fa-caret-right"></i></div>
        </header>
        <section class="year">
            <div id="calendar"></div>
        </section>
    </main>
</div>
</body>