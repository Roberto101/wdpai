<!DOCTYPE html>
<head>
    <title>Login page</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/Logo.svg">
        </div>
        <div class="login-container">
            <form class="login" action = "login" method="POST">
                <div class="messages">
                    <?php if(isset($messages)){
                        foreach($messages as $message){
                            echo $message;
                        }
                      }
                    ?>
                </div>
                <input name="email" type="text" placeholder="email@email.com">
                <input name="password" type="password" placeholder="password">
                <button type = "submit">Login</button>
                <span>No account yet? Please <a href="register" class="register_button"> Register</a></span>

            </form>
        </div>
        
    </div>
</body>