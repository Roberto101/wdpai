<!DOCTYPE html>
<head>
    <title>Plandy</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/todos.css">
    <link href="https://pl.allfont.net/allfont.css?fonts=bookman-old-style" rel="stylesheet" type="text/css" />
    <script src="https://kit.fontawesome.com/6ff9b2a121.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/public/js/menu.js" defer></script>
</head>
<body>
    <div class="base-container">
        <nav>
        <?php include('menu.php');?>
        </nav>
       <main>
           <header>
               <!--<div class="arrow"><i class="fas fa-caret-left"></i></div>-->
               <div class="menuButton" ><i class="fas fa-bars"></i></div>
               <div class="headerText">Week</div>


           </header>
           <section class="addToDo">
               <h1>Add ToDo</h1>
               <form action="addToDo" method="post">
                       <?php if(isset($messages)){
                           foreach($messages as $message){
                               echo $message;
                           }
                       }
                       ?>
                   <input name="title" type="text" placeholder="title">
                   <textarea name="description" rows="5" placeholder="description"></textarea>
                   <input name="due_date" type="date" placeholder="due_date">
                   <button type="submit">Add</button>
               </form>
           </section>
       </main>
    </div>
</body>