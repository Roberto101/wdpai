<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'],'/');
$path = parse_url($path,PHP_URL_PATH);



Routing::get('index', 'DefaultController');
Routing::get('day', 'DayController');
Routing::get('week', 'DefaultController');
Routing::get('year', 'DefaultController');
Routing::get('month', 'DefaultController');
Routing::get('todos', 'ToDosController');
Routing::get('completetodo', 'ToDosController');
Routing::get('undotodo', 'ToDosController');
Routing::post('login', 'SecurityController');
Routing::post('logout', 'SecurityController');
Routing::post('register', 'SecurityController');
Routing::post('addToDo', 'ToDosController');
Routing::post('addTask', 'TaskController');
Routing::run($path);
