<?php
session_start();
if(!(isset($_SESSION['login_user'])))
{
    $url = "http://$_SERVER[HTTP_HOST]";
    header("Location: {$url}/login");
}